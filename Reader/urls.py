from django.urls import path 
from .views import admin_read_tokoh, pemain_read_tokoh, pemain_read_warna_kulit, admin_read_warna_kulit

urlpatterns = [
    path('admin/list_tokoh', admin_read_tokoh, name='admin_read_tokoh'),
    path('pemain/list_tokoh', pemain_read_tokoh, name='pemain_read_tokoh'),
    path('admin/list_warna_kulit', admin_read_warna_kulit, name='admin_read_warna_kulit'),
    path('pemain/list_warna_kulit', pemain_read_warna_kulit, name='pemain_read_warna_kulit'),
]