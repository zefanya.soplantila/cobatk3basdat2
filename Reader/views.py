from django.shortcuts import render, HttpResponse
from utils.query import query
from otentikasi.views import get_session_data, is_authenticated, login

# Create your views here.

def admin_read_tokoh(request):
    if not is_authenticated(request):
        return login(request)
    
    if request.session['role'] == 'pemain':
        return HttpResponse("You are not authorized (must be admin)")
    
    query_result = query("SELECT * FROM tokoh")
    print(query_result)

    data = get_session_data(request)
    data['list_tokoh'] = query_result

    return render(request, 'admin_read_tokoh.html', data)


def pemain_read_tokoh(request):
    if not is_authenticated(request):
        return login(request)
    
    if request.session['role'] == 'admin':
        return HttpResponse("You are not authorized (must be pemain)")
    
    username = str(request.session['username'])
    query_result = query(f"SELECT * FROM tokoh WHERE username_pengguna = '{username}'")
    print(query_result)

    data = get_session_data(request)
    data['list_tokoh'] = query_result

    return render(request, 'pemain_read_tokoh.html', data)


def admin_read_warna_kulit(request):
    if not is_authenticated(request):
        return login(request)
    
    if request.session['role'] == 'pemain':
        return HttpResponse("You are not authorized (must be admin)")
    
    query_result = query("SELECT * FROM warna_kulit")
    print(query_result)

    data = get_session_data(request)
    data['list_warna_kulit'] = query_result
    data['idx'] = 0

    return render(request, 'admin_read_warna_kulit.html', data)


def pemain_read_warna_kulit(request):
    if not is_authenticated(request):
        return login(request)
    
    if request.session['role'] == 'admin':
        return HttpResponse("You are not authorized (must be pemain)")
    
    query_result = query("SELECT * FROM warna_kulit")
    print(query_result)

    data = get_session_data(request)
    data['list_warna_kulit'] = query_result
    data['idx'] = 0

    return render(request, 'pemain_read_warna_kulit.html', data)