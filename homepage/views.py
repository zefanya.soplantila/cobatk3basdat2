# Create your views here.
from django.shortcuts import render, HttpResponse
from otentikasi.views import is_authenticated, get_session_data, login, login_view

# Create your views here.

def admin_homepage(request):
    if is_authenticated(request):
        if request.session['role'] != 'admin':
            return HttpResponse("Anda bukanlah seorang admin :'D")
        return render(request, "admin_homepage.html", get_session_data(request))
    else:
        return login(request)

def pemain_homepage(request):
    if is_authenticated(request):
        if request.session['role'] != 'pemain':
            return HttpResponse("Anda hanyalah seorang admin :'D")
        return render(request, "pemain_homepage.html", get_session_data(request))
    else:
        return login(request)