asgiref==3.4.1
cycler==0.10.0
Cython==0.29.23
dj-database-url==0.5.0
Django==4.0.1
djangorestframework==3.13.1
djangorestframework-simplejwt==5.0.0
gunicorn==20.0.4
kiwisolver==1.3.1
postgres==4.0
psycopg2-binary==2.9.3
psycopg2-pool==1.1
PyJWT==2.3.0
pyparsing==2.4.7
pytesseract==0.3.8
python-dateutil==2.8.1
pytz==2021.3
six==1.16.0
sqlparse==0.4.2
tesseract==0.1.3
tzdata==2021.5
