from django.db import DatabaseError, IntegrityError, transaction
from collections import namedtuple
import psycopg2
from psycopg2 import Error

try:
    # Connect to an existing database

    connection = psycopg2.connect(user="faxzqefxiivkbi",
                        password="4dc5220044ce0306814a7879063f24e7d3b8c895f2cd6e8bf6345a2a519e5d69",
                        host="ec2-34-197-84-74.compute-1.amazonaws.com",
                        port="5432",
                        database="dg3f92tf4so7c")

    # connection = psycopg2.connect(user="postgres",
    #                     host="localhost",
    #                     port="5432",
    #                     database="zefanya.soplantila")

    # Create a cursor to perform database operations
    cursor = connection.cursor()

        
except (Exception, Error) as error:
    print("Error while connecting to PostgreSQL", error)


def map_cursor(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple("Result", [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def query(query_str: str):
    hasil = []
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THE_CIMS")

        try:
            cursor.execute(query_str)

            if query_str.strip().lower().startswith("select"):
                # Kalau ga error, return hasil SELECT
                hasil = map_cursor(cursor)
            else:
                # Kalau ga error, return jumlah row yang termodifikasi oleh INSERT, UPDATE, DELETE
                hasil = cursor.rowcount
                connection.commit()
        except Exception as e:
            # Ga tau error apa
            hasil = e
            transaction.rollback()

    return hasil
