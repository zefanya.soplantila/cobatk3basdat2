from django.shortcuts import render, HttpResponse
from utils.query import query
from otentikasi.views import get_session_data, is_authenticated, login
from Reader.views import pemain_read_tokoh
from django.views.decorators.csrf import csrf_exempt
import random

@csrf_exempt
def create_tokoh(request):
    if not is_authenticated(request):
        return login(request)
    
    if str(request.session['role']) == 'admin':
        return HttpResponse("You are not authorized")
    
    if request.method != 'POST':
        return create_tokoh_view(request)
    
    body = request.POST

    username_pemain = str(request.session['username'])
    nama_tokoh      = str(body.get('nama_tokoh_input'))
    jenis_kelamin   = str(body.get('jenis_kelamin_input'))
    warna_kulit     = str(body.get('kode_warna_kulit_input'))
    pekerjaan       = str(body.get('pekerjaan_input'))

    list_sifat = ['Jenius', 'Kreatif', 'Aktif', 'Materialistik', 'Lucu']
    idx_sifat  = random.randint(0, 4)

    result = query(
        f"""
        INSERT INTO tokoh VALUES
        ('{username_pemain}', '{nama_tokoh}', '{jenis_kelamin}', 'AKTIF', 0, 100, 0, 0, 
        '{warna_kulit}', 1, '{list_sifat[idx_sifat]}', '{pekerjaan}', 'RB001', 'MT001', 'RM001')
    """
    )

    print(result)

    if not type(result) == int:
        return HttpResponse("Gagal Memasukkan Data")
    
    return pemain_read_tokoh(request)


def create_tokoh_view(request):
    data = {}
    
    kode_warna_kulit = query(
        "SELECT * FROM warna_kulit"
    )

    list_pekerjaan = query(
        "SELECT * FROM pekerjaan"
    )

    data['kode_warna_kulit'] = kode_warna_kulit
    data['list_pekerjaan']   = list_pekerjaan

    return render(request, 'create_tokoh_form.html', data)