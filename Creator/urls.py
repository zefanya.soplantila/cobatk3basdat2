from django.urls import path 
from .views import create_tokoh

urlpatterns = [
    path('pemain/create_tokoh', create_tokoh, name='pemain_create_tokoh'),
]